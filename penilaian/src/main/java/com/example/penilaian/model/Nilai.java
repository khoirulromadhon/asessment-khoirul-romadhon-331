package com.example.penilaian.model;

import jakarta.persistence.*;

@Entity
@Table(name = "nilai")
public class Nilai {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "siswa_id")
    private Long siswaId;

    @ManyToOne
    @JoinColumn(name = "siswa_id", insertable = false, updatable = false)
    public Siswa siswa;

    @Column(name = "mapel_id")
    private Long mapelId;

    @ManyToOne
    @JoinColumn(name = "mapel_id", insertable = false, updatable = false)
    public Mapel mapel;

    @Column(name = "nilai")
    private int nilai;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSiswaId() {
        return siswaId;
    }

    public void setSiswaId(Long siswaId) {
        this.siswaId = siswaId;
    }

    public Siswa getSiswa() {
        return siswa;
    }

    public void setSiswa(Siswa siswa) {
        this.siswa = siswa;
    }

    public Long getMapelId() {
        return mapelId;
    }

    public void setMapelId(Long mapelId) {
        this.mapelId = mapelId;
    }

    public Mapel getMapel() {
        return mapel;
    }

    public void setMapel(Mapel mapel) {
        this.mapel = mapel;
    }

    public int getNilai() {
        return nilai;
    }

    public void setNilai(int nilai) {
        this.nilai = nilai;
    }
}
