package com.example.penilaian.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;

@Entity
@Table(name = "guru")
public class Guru {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nip")
    @NotBlank(message = "NIP Tidak Boleh Kosong !!!!!")
    private int nip;

    @Column(name = "first_name")
    @NotBlank(message = "Firstname Tidak Boleh Kosong !!!!!")
    private String first_name;

    @Column(name = "last_name")
    @NotBlank(message = "Lastname Tidak Boleh Kosong !!!!!")
    private String last_name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNip() {
        return nip;
    }

    public void setNip(int nip) {
        this.nip = nip;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
