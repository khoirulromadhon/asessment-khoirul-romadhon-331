package com.example.penilaian.repository;

import com.example.penilaian.model.Nilai;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NilaiRepositories extends JpaRepository<Nilai, Long> {
    @Query(value = "SELECT * FROM nilai WHERE siswa_id = :id", nativeQuery = true)
    List<Nilai> findSiswaNilai(Long id);
}
