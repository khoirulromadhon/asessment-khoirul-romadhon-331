package com.example.penilaian.repository;

import com.example.penilaian.model.Mapel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MapelRepositories extends JpaRepository<Mapel, Long> {
    @Query(value = "SELECT * FROM mapel WHERE LOWER(name) LIKE LOWER(:mapelName)", nativeQuery = true)
    Mapel findDuplicate(String mapelName);
}
