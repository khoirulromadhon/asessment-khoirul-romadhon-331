package com.example.penilaian.repository;

import com.example.penilaian.model.Nilai;
import com.example.penilaian.model.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SiswaRepositories extends JpaRepository<Siswa, Long> {
    @Query(value = "SELECT * FROM siswa WHERE lower(first_name) like lower(:firstname) AND lower(last_name) like (:lastname)", nativeQuery = true)
    Siswa findDuplicate(String firstname, String lastname);
}
