package com.example.penilaian.repository;

import com.example.penilaian.model.Guru;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GuruRepositories extends JpaRepository<Guru, Long> {
    @Query(value = "SELECT * FROM guru WHERE lower(first_name) like lower(:firstname) AND lower(last_name) like (:lastname)", nativeQuery = true)
    Guru findDuplicate(String firstname, String lastname);
}
