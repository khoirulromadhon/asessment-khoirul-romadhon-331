package com.example.penilaian.controller;

import com.example.penilaian.model.Mapel;
import com.example.penilaian.repository.MapelRepositories;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/mapel")
public class MapelController {
    @Autowired
    private MapelRepositories mapelRepositories;

    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("mapel/index");
        List<Mapel> mapels = this.mapelRepositories.findAll();
        view.addObject("mapels", mapels);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getMapelById(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("mapel/details");
        Optional<Mapel> mapel = this.mapelRepositories.findById(id);
        view.addObject("mapel", mapel.get());
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("mapel/form");
        Mapel mapel = new Mapel();
        view.addObject("mapel", mapel);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute @Valid Mapel mapel, BindingResult result, Model model){
        ModelAndView view = new ModelAndView("mapel/index");
        List<Mapel> mapels = this.mapelRepositories.findAll();
        Optional<Mapel> data = Optional.ofNullable(this.mapelRepositories.findDuplicate(mapel.getName()));

        if (result.hasErrors()){
            view.addObject("mapels", mapels);
            model.addAttribute("validationErrors", result.getFieldError());
            return view;
        }

        if (data.isPresent()){
            view.addObject("mapels", mapels);
            model.addAttribute("duplicateError", "Data Sudah Ada !!!!!");
            return view;
        }
        else {
            this.mapelRepositories.save(mapel);
            return new ModelAndView("redirect:/mapel");
        }
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("mapel/form");
        Optional<Mapel> mapel = this.mapelRepositories.findById(id);
        view.addObject("mapel", mapel);
        return view;
    }

    @GetMapping("deleteForm/{id}")
    public ModelAndView deleteForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("mapel/delete");
        Optional<Mapel> mapel = this.mapelRepositories.findById(id);
        view.addObject("mapel", mapel.get());
        return view;
    }

    @GetMapping("delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        this.mapelRepositories.deleteById(id);
        return new ModelAndView("redirect:/mapel");
    }
}
