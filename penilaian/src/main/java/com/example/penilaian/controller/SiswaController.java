package com.example.penilaian.controller;

import com.example.penilaian.model.Nilai;
import com.example.penilaian.model.Siswa;
import com.example.penilaian.repository.NilaiRepositories;
import com.example.penilaian.repository.SiswaRepositories;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/siswa")
public class SiswaController {
    @Autowired
    private SiswaRepositories siswaRepositories;

    @Autowired
    private NilaiRepositories nilaiRepositories;

    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("siswa/index");
        List<Siswa> siswas = this.siswaRepositories.findAll();
        view.addObject("siswas", siswas);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getSiswaById(@PathVariable("id") Long id){
        double rataRata, nilai, jmlMatkul = 0, total = 0;

        ModelAndView view = new ModelAndView("siswa/details");
        Optional<Siswa> siswa = this.siswaRepositories.findById(id);
        List<Nilai> nilais = this.nilaiRepositories.findSiswaNilai(id);

        for (Nilai n:nilais) {
            nilai = n.getNilai();
            total += nilai;
            jmlMatkul++;
        }

        rataRata = total / jmlMatkul;

        view.addObject("siswa", siswa.get());
        view.addObject("nilais", nilais);
        view.addObject("rataRata", rataRata);
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form() {
        ModelAndView view = new ModelAndView("siswa/form");
        Siswa siswa = new Siswa();
        view.addObject("siswa", siswa);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute @Valid Siswa siswa, BindingResult result, Model model){
        ModelAndView view = new ModelAndView("siswa/index");
        List<Siswa> siswas = this.siswaRepositories.findAll();
        Optional<Siswa> data = Optional.ofNullable(this.siswaRepositories.findDuplicate(siswa.getFirst_name(), siswa.getLast_name()));

        if (result.hasErrors()){
            view.addObject("siswas", siswas);
            model.addAttribute("validationErrors", result.getFieldError());
            return view;
        }


        if (data.isPresent()){
            view.addObject("siswas", siswas);
            model.addAttribute("duplicateError", "Data Sudah Ada !!!!!");
            return view;
        }
        else {
            this.siswaRepositories.save(siswa);
            return new ModelAndView("redirect:/siswa");
        }
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("siswa/form");
        Optional<Siswa> siswa = this.siswaRepositories.findById(id);
        view.addObject("siswa", siswa);
        return view;
    }

    @GetMapping("deleteForm/{id}")
    public ModelAndView deleteForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("siswa/delete");
        Optional<Siswa> siswa = this.siswaRepositories.findById(id);
        view.addObject("siswa", siswa.get());
        return view;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        this.siswaRepositories.deleteById(id);
        return new ModelAndView("redirect:/siswa");
    }
}
