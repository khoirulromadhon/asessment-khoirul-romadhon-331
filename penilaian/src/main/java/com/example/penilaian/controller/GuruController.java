package com.example.penilaian.controller;

import com.example.penilaian.model.Guru;
import com.example.penilaian.repository.GuruRepositories;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/guru")
public class GuruController {
    @Autowired
    private GuruRepositories guruRepositories;

    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("guru/index");
        List<Guru> gurus = this.guruRepositories.findAll();
        view.addObject("gurus", gurus);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getSiswaById(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("guru/details");
        Optional<Guru> guru = this.guruRepositories.findById(id);
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("guru/form");
        Guru guru = new Guru();
        view.addObject("guru", guru);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute @Valid Guru guru, BindingResult result, Model model){
        ModelAndView view = new ModelAndView("guru/index");
        List<Guru> gurus = this.guruRepositories.findAll();
        Optional<Guru> data = Optional.ofNullable(this.guruRepositories.findDuplicate(guru.getFirst_name(), guru.getLast_name()));

        if (result.hasErrors()){
            view.addObject("gurus", gurus);
            model.addAttribute("validationErrors", result.getFieldError());
            return view;
        }

        if (data.isPresent()){
            view.addObject("gurus", gurus);
            model.addAttribute("duplicateError", "Data Sudah Ada !!!!!");
            return view;
        }
        else {
            this.guruRepositories.save(guru);
            return new ModelAndView("redirect:/guru");
        }
    }

    @GetMapping("update/{id}")
    public ModelAndView update(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("guru/form");
        Optional<Guru> guru = this.guruRepositories.findById(id);
        view.addObject("guru", guru);
        return view;
    }

    @GetMapping("deleteForm/{id}")
    public ModelAndView deleteForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("guru/delete");
        Optional<Guru> guru = this.guruRepositories.findById(id);
        view.addObject("guru", guru.get());
        return view;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        this.guruRepositories.deleteById(id);
        return new ModelAndView("redirect:/guru");
    }
}
