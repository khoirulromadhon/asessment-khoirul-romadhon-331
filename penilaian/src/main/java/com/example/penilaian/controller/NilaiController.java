package com.example.penilaian.controller;

import com.example.penilaian.model.Mapel;
import com.example.penilaian.model.Nilai;
import com.example.penilaian.model.Siswa;
import com.example.penilaian.repository.MapelRepositories;
import com.example.penilaian.repository.NilaiRepositories;
import com.example.penilaian.repository.SiswaRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/nilai")
public class NilaiController {
    @Autowired
    private NilaiRepositories nilaiRepositories;

    @Autowired
    private SiswaRepositories siswaRepositories;

    @Autowired
    private MapelRepositories mapelRepositories;

    @GetMapping
    public ModelAndView index(Model model, Pageable pageable){
        ModelAndView view = new ModelAndView("nilai/index");
        List<Nilai> nilais = this.nilaiRepositories.findAll();
        view.addObject("nilais", nilais);
        return view;
    }

    @GetMapping("/{id}")
    public ModelAndView getNilaiById(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("nilai/details");
        Optional<Nilai> nilai = this.nilaiRepositories.findById(id);
        view.addObject("nilai");
        return view;
    }

    @GetMapping("/form")
    public ModelAndView form(){
        ModelAndView view = new ModelAndView("nilai/form");
        Nilai nilai = new Nilai();
        List<Siswa> siswas = this.siswaRepositories.findAll();
        List<Mapel> mapels = this.mapelRepositories.findAll();
        view.addObject("nilai", nilai);
        view.addObject("siswas", siswas);
        view.addObject("mapels", mapels);
        return view;
    }

    @PostMapping("/save")
    public ModelAndView save(@ModelAttribute Nilai nilai){
        this.nilaiRepositories.save(nilai);
        return new ModelAndView("redirect:/nilai");
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("nilai/form");
        Optional<Nilai> nilai = this.nilaiRepositories.findById(id);
        List<Siswa> siswas = this.siswaRepositories.findAll();
        List<Mapel> mapels = this.mapelRepositories.findAll();
        view.addObject("nilai", nilai);
        view.addObject("siswas", siswas);
        view.addObject("mapels", mapels);
        return view;
    }

    @GetMapping("deleteForm/{id}")
    public ModelAndView deleteForm(@PathVariable("id") Long id){
        ModelAndView view = new ModelAndView("nilai/delete");
        Optional<Nilai> nilai = this.nilaiRepositories.findById(id);
        view.addObject("nilai", nilai.get());
        return view;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id){
        this.nilaiRepositories.deleteById(id);
        return new ModelAndView("redirect:/nilai");
    }
}
