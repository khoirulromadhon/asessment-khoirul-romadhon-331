import java.util.HashMap;
import java.util.Scanner;

public class Soal16 {
    public static void Resolve(){
        Scanner inputInt = new Scanner(System.in);
        Scanner inputString = new Scanner(System.in);
        HashMap<String, Double> menus = new HashMap<>();

        Double[] people = new Double[3];
        String menu;
        char option, confirm;
        double pajak = 0, service = 0, total = 0, harga;
        boolean flag = true, alergi = false;

        while (flag){
            System.out.println("Jika menu mengandung ikan beri keterangan (ikan)");
            System.out.println("Contoh: Tuna Sandwich (ikan)");
            System.out.println("Beri .0 disetiap harga");
            System.out.println("Contoh: 90.0");
            System.out.print("Masukan Menu: ");
            menu = inputString.nextLine();
            System.out.print("Masukan Harga: ");
            harga = inputInt.nextDouble();

            menus.put(menu, harga);

            System.out.print("Apakah Ada Pesanan Lain ? (y/n) ");
            option = inputString.next().charAt(0);
            inputString.nextLine();
            
            if (option != 'y'){
                flag = false;
            }
            else {
                flag = true;
            }
        }

        for (Double h : menus.values()) {
            total += h;
            pajak += (total * 0.10);
            service += (total * 0.5);
        }

        for (int i = 0; i < people.length; i++) {
            total = 0;
            System.out.print("Apakah orang ke-" + (i+1) + " alergi ikan ? (y/n) ");
            confirm = inputString.next().charAt(0);

            if (confirm != 'y'){
                for (Double h : menus.values()) {
                    total += h;
                }

                total += (pajak + service);
                people[i] = total;
            }
            else {
                for (String m : menus.keySet()) {
                    if (m.contains("ikan")){
                        total += 0;
                    }
                    else {
                        total += menus.get(m);
                    }
                }

                total += (pajak + service);
                people[i] = total;
            }
        }

        for (int i = 0; i < people.length; i++) {
            System.out.println("Total Bayar Orang ke-" + (i+1) + " adalah " + people[i]);
        }
    }
}
