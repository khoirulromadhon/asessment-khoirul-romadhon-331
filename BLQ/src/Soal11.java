import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String kata;
        int stars;

        System.out.print("Masukan Kata: ");
        kata = input.nextLine();

        char[] chars = kata.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (chars.length %2 == 0){
                stars = chars.length / 2;
                for (int j = 0; j < chars.length + 1; j++) {
                    if (j == stars){
                        System.out.print(chars[i]);
                    }
                    else {
                        System.out.print("*");
                    }
                }
            }
            else {
                stars = chars.length / 2;
                for (int j = 0; j < chars.length; j++) {
                    if (j == stars){
                        System.out.print(chars[i]);
                    }
                    else {
                        System.out.print("*");
                    }
                }
            }
            System.out.println();
        }
    }
}
