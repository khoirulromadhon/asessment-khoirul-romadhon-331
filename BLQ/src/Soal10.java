import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String kalimat;

        System.out.print("Masukan Kalimat: ");
        kalimat = input.nextLine();

        String[] kata = kalimat.split(" ");

        for (int i = 0; i < kata.length; i++) {
            char[] chars = kata[i].toCharArray();
            for (int j = 0; j < chars.length; j++) {
                if (j == 0 || j == chars.length - 1){
                    System.out.print(chars[j]);
                }
                else {
                    System.out.print("*");
                }
            }
            System.out.print(" ");
        }

        System.out.println();
    }
}
