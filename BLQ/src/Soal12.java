import java.util.Scanner;

public class Soal12 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String deret;
        char helper;

        System.out.print("Masukan Deret: ");
        deret = input.nextLine();

        char[] chars = deret.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            for (int j = 1; j < chars.length; j++) {
                if (chars[j - 1] > chars[j]){
                    helper = chars[j - 1];
                    chars[j - 1] = chars[j];
                    chars[j] = helper;
                }
            }
        }

        System.out.print("Result: ");
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i] + " ");
        }
        System.out.println();
    }
}
