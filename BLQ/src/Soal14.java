import java.util.Scanner;

public class Soal14 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int n, helper;
        int[] derets = {3, 9, 0, 7, 1, 2, 4};

        System.out.println("Deret awal");
        for (int d : derets) {
            System.out.print(d + " ");
        }
        System.out.println();
        System.out.print("Masukan N: ");
        n = input.nextInt();

        for (int i = 0; i < n; i++) {
            helper = derets[0];
            for (int j = 0; j < derets.length; j++) {
                if (j == derets.length - 1){
                    derets[j] = helper;
                }
                else {
                    derets[j] = derets[j + 1];
                }
            }
        }

        System.out.print("Result: ");
        for (int d : derets) {
            System.out.print(d + " ");
        }
        System.out.println();
    }
}
