import java.util.Scanner;

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int n;
        int helper = 0;

        System.out.print("Masukan N: ");
        n = input.nextInt();

        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            helper += n;
            results[i] = helper;
        }

        System.out.print("Result: ");
        for (int i = 0; i < results.length; i++) {
            System.out.print(results[i] + " ");
        }
        System.out.println();
    }
}
