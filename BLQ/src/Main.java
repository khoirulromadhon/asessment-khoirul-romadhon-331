import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 22)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 22){
                System.out.println("Pilihan Tidak Tersedia !!!!!");
                System.out.print("Pilihan: ");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    Soal01.Resolve();
                    break;
                case 2:
                    Soal02.Resolve();
                    break;
                case 3:
                    Soal03.Resolve();
                    break;
                case 4:
                    Soal04.Resolve(); // Bilangan Prima
                    break;
                case 5:
                    Soal05.Resolve(); // Fibonacci
                    break;
                case 6:
                    Soal06.Resolve(); // Palindrome
                    break;
                case 7:
                    Soal07.Resolve(); // Modus, Mean, & Median
                    break;
                case 8:
                    Soal08.Resolve(); // Min & Max penjumlahan 4 Komponen
                    break;
                case 9:
                    Soal09.Resolve(); // += N
                    break;
                case 10:
                    Soal10.Resolve(); // Hanya menampilkan huruf awal dan akhir
                    break;
                case 11:
                    Soal11.Resolve(); // Kata di tengah bintang bintang
                    break;
                case 12:
                    Soal12.Resolve(); // Sorting
                    break;
                case 13:
                    Soal13.Resolve();
                    break;
                case 14:
                    Soal14.Resolve(); // Switch
                    break;
                case 15:
                    Soal15.Resolve(); // Time Conversion
                    break;
                case 16:
                    Soal16.Resolve(); // Pembagian 3 makanan di resto dengan orang yang alergi ikan
                    break;
                case 17:
                    Soal17.Resolve(); // Gunung & Lembah
                    break;
                case 18:
                    Soal18.Resolve();
                    break;
                case 19:
                    Soal19.Resolve(); // Pangram
                    break;
                case 20:
                    Soal20.Resolve();
                    break;
                case 21:
                    Soal21.Resolve();
                    break;
                case 22:
                    Soal22.Resolve();
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}