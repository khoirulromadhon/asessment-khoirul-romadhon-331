import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String stringDeret;
        int min = 0, max = 0;

        System.out.println("Deret Minimal 8 angka !!!!!");
        System.out.print("Deret: ");
        stringDeret = input.nextLine();

        String[] splitDeret = stringDeret.split(" ");
        int[] intDeret = new int[splitDeret.length];

        for (int i = 0; i < splitDeret.length; i++) {
            intDeret[i] = Integer.parseInt(splitDeret[i]);
        }

        Arrays.sort(intDeret);

        for (int i = 0; i < 4; i++) {
            min += intDeret[i];
        }

        for (int i = intDeret.length - 1; i > intDeret.length - 4; i--) {
            max += intDeret[i];
        }

        System.out.println("Min = " + min);
        System.out.println("Max = " + max);
        System.out.println();
    }
}
