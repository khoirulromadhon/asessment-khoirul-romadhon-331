import java.util.Scanner;

public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String deret;
        int temp = 0, mean, median;

        System.out.print("Masukan Deret Angka: ");
        deret = input.nextLine();

        String[] split = deret.split(" ");
        int[] deretInt = new int[split.length];

        for (int i = 0; i < deretInt.length; i++) {
            deretInt[i] = Integer.parseInt(split[i]);
        }
        
        // Mean
        for (int i = 0; i < deretInt.length; i++) {
            temp += deretInt[i];
        }

        mean = temp / 2;

        // Median
        if (deretInt.length %2 == 0){
            median = (deretInt[deretInt.length / 2] + deretInt[(deretInt.length / 2) - 1]) / 2;
        }
        else {
            median = deretInt[deretInt.length / 2];
        }

        System.out.println("Mean: " + mean);
        System.out.println("Median: " + median);
    }
}
