import java.util.Scanner;

public class Soal06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        String kata;
        int helper = 0;
        Boolean palindrome = true;

        System.out.print("Masukan kata: ");
        kata = input.nextLine();

        char[] kataChar = kata.toCharArray();
        char[] revere = new char[kataChar.length];

        for (int i = kataChar.length - 1; i >= 0 ; i--) {
            revere[helper] = kataChar[i];
            helper++;
        }

        for (int i = 0; i < kataChar.length; i++) {
            if (kataChar[i] != revere[i]){
                palindrome = false;
                break;
            }
            else {
                palindrome = true;
            }
        }

        if (palindrome == true){
            System.out.println("Kata " + kata + " adalah Palindrome");
        }
        else {
            System.out.println("Kata " + kata + " bukan Palindrome");
        }

        System.out.println();
    }
}
