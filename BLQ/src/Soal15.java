import java.util.Scanner;

public class Soal15 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Gunakan Spasi Untuk Menulis Format, contoh 03:40:44 PM");
        System.out.print("Input: ");
        String text = input.nextLine();

        String[] splits = text.split(":");

        int jam = Integer.parseInt(splits[0]);
        int menit = Integer.parseInt(splits[1]);
        String format = splits[2].substring(2);

        if (format.equalsIgnoreCase("PM") && jam != 12) {
            jam += 12;
        } else if (format.equalsIgnoreCase("AM") && jam == 12) {
            jam = 0;
        }

        System.out.print("Output: ");
        System.out.println(jam + ":" + menit + ":" + splits[2].substring(0, 2));
    }
}
