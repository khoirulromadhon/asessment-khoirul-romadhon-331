import java.util.Scanner;

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        int n;

        System.out.print("Masukan Jumlah Bilangan Fibonacci: ");
        n = input.nextInt();

        int[] fibonacci = new int[n];

        for (int i = 0; i < n; i++) {
            if (i == 0 || i == 1){
                fibonacci[i] = 1;
            }
            else {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            }
        }

        System.out.println(n + " bilangan fibonacci pertama adalah: ");
        for (int i = 0; i < fibonacci.length; i++) {
            System.out.print(fibonacci[i] + " ");
        }
        System.out.println();
    }
}
